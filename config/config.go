package config

import "time"

type Config struct {
	Server      ServerConfig
	DbConfig    DbConfig
	Log         Log
	RedisConfig RedisConfig
	HTTP        struct {
		TimeOut             time.Duration
		MaxIdleConns        int
		MaxIdleConnsPerHost int
		MaxConnsPerHost     int
	}
}

type ServerConfig struct {
	Port         int
	WriteTimeout int
	ReadTimeout  int
	IdleTimeout  int
}

type Log struct {
	Level string
}

type RedisConfig struct {
	Mode                         string
	Host                         string
	Port                         string
	Password                     string
	DB                           int
	RefExpireDurationMinutes     time.Duration
	BondProfileExpirationMinutes time.Duration
	StateExpirationMinutes       time.Duration
	Sentinel                     struct {
		MasterName string
		Addrs      []string
	}
}

type DbConfig struct {
	DBHost          string
	DBPort          string
	DBUsername      string
	DBPassword      string
	DBName          string
	MaxOpenConns    int32
	MaxConnLifetime int32
}
