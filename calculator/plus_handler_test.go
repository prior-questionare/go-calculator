package calculator

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestPlusMustOK(t *testing.T) {
	logger := zap.NewExample()
	plusFunc := NewPlusFunc()
	result := plusFunc(logger, 8, 7)

	assert.Equal(t, result, int64(15))
}

func TestHandlerMustOK(t *testing.T) {
	logger := zap.NewExample()
	plusSUCCESSFunc := func(logger *zap.Logger, number1, number2 int64) int64 {
		return 5
	}
	setRedisSUCCESSFunc := func(ctx context.Context, logger *zap.Logger, key string, result int64) error {
		return nil
	}
	getRedisSUCCESSFunc := func(ctx context.Context, logger *zap.Logger, key string) (int64, error) {
		return 10, nil
	}

	input := Input{
		Number1: 1,
		Number2: 4,
	}

	inpJson, _ := json.Marshal(input)

	body := bytes.NewReader(inpJson)

	handler := NewPlusHandler(logger, plusSUCCESSFunc, getRedisSUCCESSFunc, setRedisSUCCESSFunc)

	req := httptest.NewRequest("POST", "http://example/api", body)

	rec := httptest.NewRecorder()
	handler.ServeHTTP(rec, req)

	resp := rec.Result()

	assert.Equal(t, resp.StatusCode, http.StatusOK)

}

func TestHandlerSetRedisFailed(t *testing.T) {
	logger := zap.NewExample()
	plusSUCCESSFunc := func(logger *zap.Logger, number1, number2 int64) int64 {
		return 5
	}
	setRedisSUCCESSFunc := func(ctx context.Context, logger *zap.Logger, key string, result int64) error {
		return errors.New("cannot set redis")
	}
	getRedisSUCCESSFunc := func(ctx context.Context, logger *zap.Logger, key string) (int64, error) {
		return 10, nil
	}

	input := Input{
		Number1: 1,
		Number2: 4,
	}

	inpJson, _ := json.Marshal(input)

	body := bytes.NewReader(inpJson)

	handler := NewPlusHandler(logger, plusSUCCESSFunc, getRedisSUCCESSFunc, setRedisSUCCESSFunc)

	req := httptest.NewRequest("POST", "http://example/api", body)

	rec := httptest.NewRecorder()
	handler.ServeHTTP(rec, req)

	resp := rec.Result()

	assert.NotEqual(t, resp.StatusCode, http.StatusOK)

}
