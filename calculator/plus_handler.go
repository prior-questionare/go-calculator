package calculator

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/prior-questionare/go-calculator/common"
	"go.uber.org/zap"
)

type Input struct {
	Number1 int64 `json:"number_1"`
	Number2 int64 `json:"number_2"`
}

func (i *Input) validate() error {
	unixTime := time.Now().Unix() % 10
	if i.Number1 == i.Number2 {
		return errors.New("number_1 must not eq number_2")
	}
	if i.Number1 == unixTime || i.Number2 == unixTime {
		return errors.New(fmt.Sprintf("number_1 or number_2 must not unixTime[%v]", unixTime))
	}

	return nil
}

func NewPlusHandler(logger *zap.Logger,
	plusFunc PlusFunc,
	getRedisFunc GetRedisFunc,
	setRedisFunc SetRedisFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		enc := json.NewEncoder(w)

		var input Input
		err := json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			logger.Error("cannot decode parameter")
			w.WriteHeader(http.StatusBadRequest)
			resp := common.Response{
				Code:    901,
				Message: "cannot decode parameter",
			}
			enc.Encode(resp)
			return
		}

		err = input.validate()
		if err != nil {
			logger.Error(err.Error())
			w.WriteHeader(http.StatusBadRequest)
			resp := common.Response{
				Code:    901,
				Message: err.Error(),
			}
			enc.Encode(resp)
			return
		}

		redisKey := fmt.Sprintf("%s:%d:%d", "PLUS", input.Number1, input.Number2)

		result, err := getRedisFunc(r.Context(), logger, redisKey)
		if err != nil {
			result = plusFunc(logger, input.Number1, input.Number2)
			logger.Info(fmt.Sprintf("key=%s, value=%v", redisKey, result))
			err = setRedisFunc(r.Context(), logger, redisKey, result)
			if err != nil {
				logger.Error("cannot set redis")
				w.WriteHeader(http.StatusBadRequest)
				resp := common.Response{
					Code:    901,
					Message: "cannot set redis",
				}
				enc.Encode(resp)
				return
			}
		}

		resp := common.Response{
			Code:    0,
			Message: "Success",
			Data: common.ResponseData{
				Result: result,
			},
		}

		w.WriteHeader(http.StatusOK)
		enc.Encode(&resp)
		return
	})
}

type PlusFunc func(logger *zap.Logger, number1, number2 int64) int64

func NewPlusFunc() PlusFunc {
	return func(logger *zap.Logger, number1, number2 int64) int64 {
		return number1 + number2
	}
}

type SetRedisFunc func(ctx context.Context, logger *zap.Logger, key string, result int64) error

func NewSetRedisFunc(cache *redis.Client, exp time.Duration) SetRedisFunc {
	return func(ctx context.Context, logger *zap.Logger, key string, result int64) error {
		err := cache.Set(ctx, key, result, exp).Err()
		return err
	}
}

type GetRedisFunc func(ctx context.Context, logger *zap.Logger, key string) (int64, error)

func NewGetRedisFunc(cache *redis.Client) GetRedisFunc {
	return func(ctx context.Context, logger *zap.Logger, key string) (int64, error) {
		result, err := cache.Get(ctx, key).Int64()
		return result, err
	}
}
