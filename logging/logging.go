package logging

import (
	"context"
	"fmt"
	"gitlab.com/prior-questionare/go-calculator/config"
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type keyType string

const (
	key keyType = "logger"
)

var logger = zap.NewExample()

func InitLogger(log config.Log) func() {
	ec := zap.NewProductionEncoderConfig()
	ec.EncodeTime = zapcore.ISO8601TimeEncoder

	cfg := zap.NewProductionConfig()

	cfg.EncoderConfig = ec

	if log.Level == "debug" {
		cfg.Level.SetLevel(zap.DebugLevel)
	}

	l, err := cfg.Build()
	if err != nil {
		panic(err)
	}

	hn, _ := os.Hostname()
	if hn == "" {
		hn = "unknown"
	}

	logger = l.With(zap.String("hostname", hn))
	return func() {
		l.Sync() //nolint errorcheck
	}
}

func NewLogger() *zap.Logger {
	return logger.With()
}

func NewLoggerWithContext(ctx context.Context) *zap.Logger {
	rl := ContextRequestLogging(ctx)
	fmt.Printf("%v\n", rl)
	return logger.With(
		zap.String("trace_id", rl.TraceID),
		zap.String("parent_id", rl.ParentID),
		zap.String("span_id", rl.SpanID),
		zap.String("remote_address", rl.RemoteAddress),
		zap.String("action", rl.Action),
	)
}

func TraceParent(ctx context.Context) string {
	return ContextRequestLogging(ctx).TraceParent
}

func ExecutionTime(start time.Time, name string, l *zap.Logger) {
	elapse := time.Since(start)
	l.With(zap.Int64("duration", elapse.Milliseconds())).Info(fmt.Sprintf("%s took %s", name, elapse))
}
