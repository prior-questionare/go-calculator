package logging

import (
	"context"
	"encoding/json"
)

type RequestLogging struct {
	Action        string `json:"action"`
	TraceID       string `json:"trace_id"`
	ParentID      string `json:"parent_id"`
	SpanID        string `json:"span_id"`
	RemoteAddress string `json:"remote_address"`
	Tag           string `json:"tag"`
	Msg           string `json:"msg"`
	Duration      string `json:"duration"`
	TraceParent   string `json:"traceparent"`
}

func (reqLog *RequestLogging) InComing(msg string) string {
	reqLog.Tag = "input"
	reqLog.Msg = msg
	return reqLog.toJson()
}

func (reqLog *RequestLogging) OutGoing(msg string) string {
	reqLog.Tag = "output"
	reqLog.Msg = msg
	return reqLog.toJson()
}

func (reqLog *RequestLogging) Error(msg string) string {
	reqLog.Tag = "error"
	reqLog.Msg = msg
	return reqLog.toJson()
}

func (reqLog *RequestLogging) toJson() string {
	json, _ := json.Marshal(reqLog)
	return string(json)
}

// RL Get RequestLogging from context
func ContextRequestLogging(ctx context.Context) *RequestLogging {
	v := ctx.Value(key)
	if v == nil {
		return &RequestLogging{}
	}

	l, ok := v.(*RequestLogging)
	if ok {
		return l
	}

	return &RequestLogging{}
}
