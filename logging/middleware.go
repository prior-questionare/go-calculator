package logging

import (
	"context"
	"crypto/rand"
	"io"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

func TraceAndLoggingMiddleware() mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			funcName := strings.Replace(r.URL.String(), "/", "", 1)
			rl := &RequestLogging{
				Action:        funcName,
				RemoteAddress: r.RemoteAddr,
				//TraceID:       newTp.Span.TraceID.String(),
				//ParentID:      parentID,
				//SpanID:        newTp.Span.SpanID.String(),
				//TraceParent:   newTp.String(),
			}

			r.Body = &DecodeReader{r.Body, rl}
			next.ServeHTTP(&EncodeWriter{w, rl}, r.WithContext(context.WithValue(r.Context(), key, rl)))
		})
	}
}

func generateNonce(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}

type EncodeWriter struct {
	http.ResponseWriter
	loggin *RequestLogging
}

func (w *EncodeWriter) Write(b []byte) (int, error) {
	return w.ResponseWriter.Write(b)
}

type DecodeReader struct {
	io.ReadCloser
	loggin *RequestLogging
}

func (r *DecodeReader) Read(p []byte) (int, error) {
	return r.ReadCloser.Read(p)
}
