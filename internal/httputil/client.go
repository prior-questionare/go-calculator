package httputil

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

type HTTPPostRequestFactoryFunc func(timeout time.Duration, certPool *x509.CertPool) HTTPPostRequestFunc
type HTTPPostRequestFunc func(body []byte, headers map[string]string) ([]byte, error)

func InitHttpClient(certs []string, timeout time.Duration, maxIdleConns, maxIdleConnsPerHost, maxConnsPerHost int) *http.Client {
	certPool := x509.NewCertPool()
	for i := range certs {
		certPool.AppendCertsFromPEM([]byte(certs[i]))
	}
	client := &http.Client{
		Timeout: timeout,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: certPool,
			},
			MaxIdleConns:        maxIdleConns,
			MaxIdleConnsPerHost: maxIdleConnsPerHost,
			MaxConnsPerHost:     maxConnsPerHost,
		},
	}
	return client
}

func NewHttpPostCall(client *http.Client, HTTPAPIServerAddress, path, token string, toggle bool) HTTPPostRequestFunc {
	if !toggle {
		return func(message []byte, headers map[string]string) ([]byte, error) {
			return []byte{}, nil
		}
	}

	return func(message []byte, headers map[string]string) ([]byte, error) {
		endpoint := HTTPAPIServerAddress + path

		req, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(message))
		if err != nil {
			return nil, errors.Wrap(err, "Unable to New http Request")
		}

		req.Header.Add("Content-Type", "application/json")
		if len(token) > 0 {
			req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
		}
		for key, val := range headers {
			req.Header.Add(key, val)
		}

		res, err := client.Do(req)
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("Unable to request %s", endpoint))
		}

		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return nil, errors.Wrap(err, "Unable to New http Request ioutil")
		}

		if res.StatusCode != http.StatusOK {
			return nil, fmt.Errorf("request error with statusCode: %d, msg: %s", res.StatusCode, body)
		}
		return body, nil
	}
}

type CommonHttpResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
