package db

import (
	"context"
	"fmt"
	"gitlab.com/prior-questionare/go-calculator/config"
	"gitlab.com/prior-questionare/go-calculator/logging"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

func NewPool(config config.DbConfig, dbName string) (*pgxpool.Pool, error) {
	psqlInfo := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config.DBHost,
		config.DBPort,
		config.DBUsername,
		config.DBPassword,
		dbName,
	)

	pgxconfig, err := pgxpool.ParseConfig(psqlInfo)
	if err != nil {
		return nil, err
	}
	pgxconfig.MaxConns = config.MaxOpenConns
	pgxconfig.MaxConnLifetime = time.Duration(config.MaxConnLifetime) * time.Second

	connection, err := pgxpool.ConnectConfig(context.Background(), pgxconfig)
	if err != nil {
		return nil, err
	}

	logging.NewLogger().Info(fmt.Sprintf("Database %s starting... ", dbName))
	return connection, nil
}
