package cache

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/prior-questionare/go-calculator/config"
	"time"

	"github.com/go-redis/redis/v8"
)

func New(ctx context.Context, config config.RedisConfig) (*redis.Client, error) {
	var redisClient *redis.Client
	if config.Mode == "normal" {
		redisClient = redis.NewClient(&redis.Options{
			Addr:     config.Host + ":" + config.Port,
			Password: config.Password,
			DB:       config.DB,
		})
	} else if config.Mode == "sentinel" {
		redisClient = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName:    config.Sentinel.MasterName,
			SentinelAddrs: config.Sentinel.Addrs,
			Password:      config.Password,
			DB:            config.DB,
		})
	} else {
		return nil, fmt.Errorf("redis config : wrong redis mode, Can be [normal, sentinel]")
	}

	ctxRedis, cancelRedis := context.WithTimeout(context.Background(), time.Duration(5)*time.Second)
	defer cancelRedis()

	cliCh := make(chan string)
	errCh := make(chan error)

	go func() {
		cli, err := redisClient.Ping(ctx).Result()
		if err != nil {
			errCh <- err
		}
		if cli == "PONG" {
			cliCh <- cli
		}
	}()

	select {
	case <-cliCh:
		return redisClient, nil
	case errMsg := <-errCh:
		return nil, errors.New("Cannot connect to redis : " + errMsg.Error())
	case <-ctxRedis.Done():
		return nil, errors.New("connect to redis timeout")
	}

}
