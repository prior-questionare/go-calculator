package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"gitlab.com/prior-questionare/go-calculator/app"
	"gitlab.com/prior-questionare/go-calculator/calculator"
	"gitlab.com/prior-questionare/go-calculator/config"
	"gitlab.com/prior-questionare/go-calculator/internal/cache"
	"gitlab.com/prior-questionare/go-calculator/logging"
	"gitlab.com/prior-questionare/go-calculator/version"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

var (
	httpServer *http.Server
)

func JSONMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("content-type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func main() {
	cfg, err := initConfig()
	if err != nil {
		log.Fatal(err)
	}
	zapper := logging.InitLogger(cfg.Log)
	defer zapper()
	logger := logging.NewLogger()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	g, ctx := errgroup.WithContext(ctx)

	redisClient, err := cache.New(ctx, cfg.RedisConfig)
	if err != nil {
		logger.Fatal("server connect to redis", zap.Error(err))
	}
	defer func() {
		err = redisClient.Close()
		if err != nil {
			logger.Fatal("closing redis connection error", zap.Error(err))
		}
	}()

	//dbPool, err := db.NewPool(cfg.DbConfig, cfg.DbConfig.DBName)
	//if err != nil {
	//	logger.Fatal("server connect to db bond secondary", zap.Error(err))
	//}
	//logger.Debug(fmt.Sprintf("DB Connected to %s", cfg.DbConfig.DBName))
	//defer dbPool.Close()

	//httpClient := httputil.InitHttpClient(
	//	[]string{},
	//	cfg.HTTP.TimeOut,
	//	cfg.HTTP.MaxIdleConns,
	//	cfg.HTTP.MaxIdleConnsPerHost,
	//	cfg.HTTP.MaxConnsPerHost,
	//)

	r := mux.NewRouter()
	api := r.PathPrefix("/api").Subrouter()
	api.Use(logging.TraceAndLoggingMiddleware())
	api.Use(JSONMiddleware)

	r.HandleFunc("/version", versionHandler)

	api.Handle("/plus", calculator.NewPlusHandler(logger,
		calculator.NewPlusFunc(),
		calculator.NewGetRedisFunc(redisClient),
		calculator.NewSetRedisFunc(redisClient, time.Minute*5))).Methods(http.MethodPost)

	httpServer = &http.Server{
		Addr: fmt.Sprintf(":%d", cfg.Server.Port),

		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * time.Duration(cfg.Server.WriteTimeout),
		ReadTimeout:  time.Second * time.Duration(cfg.Server.ReadTimeout),

		Handler: r, // Pass our instance of gorilla/mux in.
	}

	logger.Info(fmt.Sprintf(
		"Starting the service...commit: %s, build time: %s, release: %s",
		version.GitCommit, version.Buildtime, version.Version,
	))

	logger.Info(fmt.Sprintf("server serve on :%d\n", cfg.Server.Port))

	g.Go(func() error {
		if err := httpServer.ListenAndServe(); err != http.ErrServerClosed {
			logger.Error("HTTP Server listen failed", zap.Error(err))
			return err
		}
		return nil
	})

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)
	defer signal.Stop(interrupt)

	select {
	case <-interrupt:
		break
	case <-ctx.Done():
		break
	}

	logger.Info("received shutdown signal")

	cancel()

	shutdownCtx, shutdownCancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdownCancel()
	if httpServer != nil {
		logger.Info("shutdown httpServer ...")
		_ = httpServer.Shutdown(shutdownCtx)
	}

	err = g.Wait()
	if err != nil {
		logger.Fatal("server running ", zap.Error(err))
	}
}

func versionHandler(w http.ResponseWriter, r *http.Request) {

	logger := logging.NewLoggerWithContext(r.Context())
	defer logging.ExecutionTime(time.Now(), fmt.Sprintf("Done send to %s API", r.RequestURI), logger)

	version := "0.0.1"
	logger.Info(fmt.Sprintf("version=%v", version))
	resp := app.NewResponse(0, "success", version)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(&resp)
}

func initConfig() (*config.Config, error) {
	viper.SetDefault("LOG.LEVEL", "debug")

	viper.SetDefault("SERVER.PORT", 9081)
	viper.SetDefault("SERVER.WRITETIMEOUT", 15)
	viper.SetDefault("SERVER.READTIMEOUT", 15)
	viper.SetDefault("SERVER.IDLETIMEOUT", 60)

	viper.SetDefault("REDISCONFIG.MODE", "normal")
	viper.SetDefault("REDISCONFIG.HOST", "localhost")
	viper.SetDefault("REDISCONFIG.PORT", "6379")
	viper.SetDefault("REDISCONFIG.PASSWORD", "")
	viper.SetDefault("REDISCONFIG.DB", "4")
	viper.SetDefault("REDISCONFIG.REFEXPIREDURATIONMINUTES", "2m")
	viper.SetDefault("REDISCONFIG.STATEEXPIRATIONMINUTES", "10m")
	viper.SetDefault("REDISCONFIG.SENTINEL.MASTERNAME", "redis-cluster")
	viper.SetDefault("REDISCONFIG.SENTINEL.ADDRS", "10.9.104.144:16379,10.9.104.145:16380,10.9.104.146:16381")

	viper.SetDefault("DBCONFIG.DBHOST", "localhost")
	viper.SetDefault("DBCONFIG.DBPORT", "5432")
	viper.SetDefault("DBCONFIG.DBUSERNAME", "postgres")
	viper.SetDefault("DBCONFIG.DBPASSWORD", "postgres")
	viper.SetDefault("DBCONFIG.DBNAME", "postgres")
	viper.SetDefault("DBCONFIG.MAXOPENCONNS", "100")
	viper.SetDefault("DBCONFIG.MAXCONNLIFETIME", "300")

	viper.SetDefault("HTTP.TIMEOUT", "10s")
	viper.SetDefault("HTTP.MAXIDLECONNS", 100)
	viper.SetDefault("HTTP.MAXIDLECONNSPERHOST", 100)
	viper.SetDefault("HTTP.MAXCONNSPERHOST", 100)

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	var c config.Config

	err := viper.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	return &c, nil
}
